# ===================================
# Grenoble, 18/04/2024
# dconv_multiV5, versione 5
# Conversione dati trasmissione o fluorescenza 
# Prende la configurazione da un file di configurazione 
# Author: Alessandro Puri, Jacopo Orsilli
# ===================================

from tkinter.filedialog import askopenfilenames, askopenfilename
from tkinter import ttk, StringVar, Label, Scrollbar, Canvas, Frame, Button, Tk
from numpy import sqrt, radians, sin
from pandas import read_csv
from sys import exit
import importlib.machinery
from datetime import datetime
from re import split
from xraylib import K_SHELL, L3_SHELL, L2_SHELL, L1_SHELL, EdgeEnergy
from math import asin

class ScrollableFrame(ttk.Frame):
    def __init__(self, container, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        canvas = Canvas(self, *args, **kwargs, bg = 'white', border= 0)
        scrollbar = Scrollbar(self, orient = 'vertical', bg = 'white', command = canvas.yview)
        self.scrollable_frame = Frame(canvas, bg = 'white')

        self.scrollable_frame.bind(
            "<Configure>",
            lambda e: canvas.configure(
                scrollregion=canvas.bbox('all')
            )
        )

        canvas.create_window((0, 0), window = self.scrollable_frame, anchor = 'nw')
        canvas.configure(yscrollcommand=scrollbar.set)
        canvas.pack(side = 'left', fill = 'both', expand = True)
        scrollbar.pack(side = 'right', fill = 'y')

def read_dat(path:str):
    """read the dat file related with the edf map
    ----------------
    Input
    path (str): path to the dat file

    ----------------
    Output
    datfile: file with all the counters
    meta: dictionary with metadata of the acquisition
    """
    dropColumns = ['Epoch', 'epitch', 'srcur', 'VPOS_enc', 'HPOS2_enc', 'RBENCH_enc', 'EBENCH_enc', 'ROT_enc', 'HPOS1_enc', 
                   'sdd', 'ebraggc', 'e1pv',  'e1sp',  'e1op',  'e1wsp', 'Seconds', 'Seconds.1']
    MetaData = {}

    with open(path, 'r') as f:
        Lines = f.readlines()

    HeadPos = [l[0] for l in enumerate(Lines) if '#L' in l[1]]

    Date = [l for l in Lines if '#D' in l]              #store the date of measurement
    Date = datetime.strptime(Date[0][3:-1], '%a %b %d %H:%M:%S %Y')

    Xtal = [l for l in Lines if '#P4' in l]
    Xtal = float(split(' ', Xtal[0])[4])
    
    Footer = len([l for l in Lines if '#C' in l])
    
    if Xtal > 0:
        Xtal = '311'
    else:
        Xtal = '111'

    #stores te counters and fix the columns
    DatFile = read_csv(path, header = HeadPos[0]-1, sep= r'\s+', engine = 'python', skipfooter = Footer)
    DatFile.columns =  list(DatFile.columns.values)[1:] + ['None']
    DatFile = DatFile.dropna(axis = 1)
    DatFile = DatFile.drop(dropColumns, axis = 1)
    if 'mono' in DatFile.columns:
        DatFile = DatFile.drop('mono', axis = 1)
    if 'energy' in DatFile.columns:
        DatFile = DatFile.drop('energy', axis = 1)

    MetaData['Date'] = Date
    MetaData['Xtal'] = Xtal

    return DatFile, MetaData

def calcEBragg(El, Edge, Xtal, a0):
    Shells = {'K': K_SHELL,
              'L3': L3_SHELL,
              'L2': L2_SHELL,
              'L1': L1_SHELL             
             }
    EdgeE = EdgeEnergy(El, Shells[Edge])*1000
    ene1ang = 12398.42014541
    stepEnc = 0.0000000218166156499

    XtalD = {'111': sqrt(3),
             '311': sqrt(11)}
    
    d =  a0/XtalD[Xtal]
    conv = ene1ang/(2*d)
    mono = asin(conv/EdgeE)
    ebragg = int(round(mono/stepEnc))
    return ebragg

factorDict = {'311': 20560.4204,
              '111': 10737.3294,
              '333': 31122.9881}

a0 = 5.4185*1.00202
global openConfig
openConfig = False

def openifile():
    global mypaths
    myfiles = askopenfilenames(title='Choose a file')
    mypaths = list(myfiles)
    if len(list(myfiles)) > 0:
        pathstr = 'Importing:\n'
        for path in mypaths:
            pathstr += path + ';\n'
        feedbacktext.set(pathstr)
    else:
        feedbacktext.set('No dat file imported')

def loadConfig():
    global CalibEl, CalibEdge, eBraggExp, columnsXiaOn, columnsXiaOFF, openConfig
    configFile = askopenfilename(title='Choose a configuration file')
    myvars = importlib.machinery.SourceFileLoader('myvars', configFile).load_module()
    CalibEl = myvars.CalibElZ
    CalibEdge = myvars.CalibEdge
    eBraggExp = myvars.eBraggExp
    columnsXiaOn = myvars.columnsXiaOn
    columnsXiaOFF = myvars.columnsXiaOff

    calibElText.set(str(CalibEl))
    calibShellText.set(CalibEdge)
    eBraggText.set(str(eBraggExp))
    configPathText.set(configFile)
    openConfig = True

def convert():
    if openConfig == True:
        for filepath in mypaths:
            filename = filepath[:-4]
            try:
                data, metadata = read_dat(filepath)
            
            except:
                feedbacktext.set('Error in reading %s' %filepath)
                break

            if len(data.columns) >= 20:
                selcolumns = columnsXiaOn
            else:
                selcolumns = columnsXiaOFF

            factor = factorDict[metadata['Xtal']]
            eBraggTheo = calcEBragg(CalibEl, CalibEdge, metadata['Xtal'], a0)
            th0 = eBraggTheo - eBraggExp
            EBraggEnergy = factor/a0/sin(radians((data['ebragg'] + th0)/800000.0))
            
            data = data.drop('ebragg', axis = 1)
            outdata  = data.copy()
            outdata = outdata.loc[:, selcolumns]
            outdata.insert(0, '# BraggEnergy', EBraggEnergy)

            with open(filename + '.xdi', 'w') as xdifile:
                xdifile.write('# XDI/1.0 GSE/1.0\n')
                xdifile.write('# Scan-start_time: %s\n' % (metadata['Date'].isoformat()))
                xdifile.write('# Column.1: energy eV\n')
                for i, col in enumerate(outdata.columns[1:]):
                    xdifile.write('# Column.%d: %s\n' % (i+2, col))
                xdifile.write('# Mono.name: Si %s\n' % (metadata['Xtal'])) 
                xdifile.write('# Mono.d_spacing: 3.134692\n') 
                xdifile.write('# Mono.notes: LNT cooled\n') 
                xdifile.write('# Beamline.name: BM08-LISA\n')
                xdifile.write('# Facility.name: ESRF\n')
                xdifile.write('# Facility.xray_source: Bending magnet\n')
                xdifile.write(outdata.to_string(index = False).strip().replace('  ', ' '))
            
            feedbacktext.set('Conversion completed')

    else:
        configPathText.set('Configuration file missing')

def quit():
    exit()                       

window=Tk()

calibElText = StringVar()
calibElText.set('n.a.')
calibShellText = StringVar()
calibShellText.set('n.a.')
eBraggText = StringVar()
eBraggText.set('n.a.')
configPathText = StringVar()
configPathText.set('n.a.')
feedbacktext = StringVar()
feedbacktext.set('No dat file imported')

# add widgets here
lbl1 = Label(window, text='LISA data reduction Ver. 5.0', fg = 'blue', bg = 'white', font = ("Helvetica", 20))
lbl1.place(relx = 0.5, rely = 0.07, anchor = 'center')
lbl2 = Label(window, text = 'A.P. - J.O. 2024', fg = 'blue',  bg = 'white', font = ("Helvetica", 10))
lbl2.place(relx = 0.5, rely = 0.13, anchor = 'center')

lbl3 = Label(window, text = 'Z config:', fg = 'black', bg = 'white', font = ("arial", 10)) 
lbl3.place(relx = 0.2, rely = 0.3, anchor = 'ne')
lbl3val = Label(window, textvariable = calibElText, fg = 'black', bg = 'white', font = ("helvetica", 10)) 
lbl3val.place(relx = 0.22, rely = 0.3, anchor = 'nw')
lbl4 = Label(window, text = 'Shell config:', fg = 'black', bg = 'white', font = ("Helvetica", 10)) 
lbl4.place(relx = 0.45, rely = 0.3, anchor = 'ne')
lbl4val = Label(window, textvariable = calibShellText, fg = 'black', bg = 'white', font = ("Helvetica", 10)) 
lbl4val.place(relx = 0.47, rely = 0.3, anchor = 'nw')
lbl5 = Label(window, text = 'eBragg exp:', fg = 'black', bg = 'white', font = ("Helvetica", 10)) 
lbl5.place(relx = 0.7, rely = 0.3, anchor = 'ne')
lbl5val = Label(window, textvariable = eBraggText, fg = 'black', bg = 'white', font = ("Helvetica", 10)) 
lbl5val.place(relx = 0.72, rely = 0.3, anchor = 'nw')

lbl6 = Label(window, text = 'Config path:', fg = 'black', bg = 'white', font = ("Helvetica", 10)) 
lbl6.place(relx = 0.2, rely = 0.4, anchor = 'ne')
lbl6Frame = ScrollableFrame(window, height= 40, width = 400)
lbl6Frame.place(relx = 0.22, rely = 0.4, anchor = 'nw')
lbl6val = Label(lbl6Frame.scrollable_frame, textvariable = configPathText, fg = 'black', bg = 'white', font = ("Helvetica", 10), anchor = 'w', wraplength = 400, justify= 'left').pack() 

lbl7 = Label(window, text = 'dat files:', fg = 'black', bg = 'white', font = ("Helvetica", 10))
lbl7.place(relx = 0.2, rely = 0.55, anchor = 'ne')
lbl7Frame = ScrollableFrame(window, height= 90, width = 400)
lbl7Frame.place(relx = 0.22, rely = 0.55, anchor = 'nw')
lbl7val = Label(lbl7Frame.scrollable_frame, textvariable = feedbacktext, fg = 'black', bg = 'white', font = ("Helvetica", 10), anchor = 'w', wraplength = 400, justify= 'left').pack()

btn1 = Button(text = 'Load Config.', font = ("arial", 10), relief = 'raised', command = loadConfig, borderwidth= 0, height = 1, width = 10, bg = 'white', fg = 'red')
btn1.place(relx = 0.5, rely = 0.25, anchor = 'center')
btn2 = Button(text ='Open File', font = ("arial", 10), command = openifile, borderwidth= 0, height = 1, width = 10, bg = 'white')
btn2.place(relx = 0.45, rely = 0.85, anchor = 'e')
btn3 = Button(text = 'Convert', font = ("arial", 10), fg = 'red', command = convert, borderwidth= 0, height = 1, width = 10, bg = 'white')
btn3.place(relx = 0.55, rely = 0.85, anchor = 'w')
btn4 = Button(text = 'Quit', font = ("arial", 10), command = quit, borderwidth= 0, height = 1, width = 10, bg = 'white')
btn4.place(relx = 0.5, rely = 0.95, anchor = 'center')
window.title('Data converter')
window.geometry("600x420")
window.configure(bg = 'white')
window.mainloop()
