#  LISA DATA REDUCTION VER. 5.0 
- Grenoble, 18/04/2024				     	
- dconv_multiV5, version 5.0					
- Trasmission and Fluorescence data conversion 		
- Authors: Alessandro Puri, Jacopo Orsilli			

# Python version

PYTHON VERSION 3.12.2

PACKAGES NEEDED 

- numpy __________ ver. 1.26.4
- pandas _________ ver. 2.2.20
- tk _____________ ver. 8.6.13
- xraylib ________ ver. 4.1.40


# Action

The program converts one or more '.dat' files acquired at the LISA beamline in '.xdi' standard file format
The program also performs an energy correction.

# How to run the program:
1. Load the '.txt' configuration file by clicking on the button 'Load Config'
   - The Element, K-edge, and experimental eBragg values will be printed
   - The path of the configuration file will be displayed in the first box

2. Open the '.dat' files to convert by clicking on the button 'Open File'
   - The path to the chosen files will be displayed in the lower box

3. Press on 'Convert'
   - At the end of the conversion the lower box will show 'Conversion Completed'
   - If a file is corrupt a message in the lower box will warn you and the conversion will be blocked
     also the path to the corrupt file will be shown. 
     Check if:
	a. The file imported is the right file
	b. In the file is stored only one acquisition. 
	
	NB. The code automatically will ignore the comments (#C in the .dat file) but if more acquisition are stored
	in the same .dat file the conversion will not work. Please create a new .dat file with the pending acquisition 
	and convert them separately

   - If the configuration file is missing the conversion will not take place

4. The '.xmi' files are created in the same folder of the '.dat' files.
